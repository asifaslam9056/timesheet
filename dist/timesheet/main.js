(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/AddTimeLog/addtimelog.component.html":
/*!******************************************************!*\
  !*** ./src/app/AddTimeLog/addtimelog.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n\r\n\r\n<div class=\"mb-5\">\r\n    <h1>Add New Record</h1>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<div class=\"row mt-2\">\r\n    <div class=\"col-lg-1\"><label class=\"label-control\">Employee:</label></div>\r\n    <div class=\"col-lg-4\">\r\n\r\n        <select [(ngModel)]='empSheet.EmployeeId' class=\"form-control\" disabled>\r\n    <option *ngFor=\"let emp of employeesList\" [ngValue]=\"emp.id\"  [selected]=\"emp.id == empSheet.EmployeeId\">\r\n        {{emp.name}}\r\n    </option>\r\n</select>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"row mt-2\">\r\n    <div class=\"col-lg-1\"><label class=\"label-control\">Task:</label></div>\r\n\r\n\r\n    <div class=\"col-lg-4\">\r\n        <select [(ngModel)]='empSheet.TaskId' class=\"form-control\" (ngModelChange)='onOptionsSelected($event)'>\r\n    <option *ngFor=\"let task of taskList\" [ngValue]=\"task.id\"  [selected]=\"task.id == empSheet.TaskId\">\r\n        {{task.name}}\r\n    </option>\r\n</select>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n<div class=\"row mt-2\">\r\n    <div class=\"col-lg-1\"><label class=\"label-control\">Task Date:</label></div>\r\n    <div class=\"col-lg-4\">\r\n\r\n        <input type=\"date\" class=\"form-control\" [max]=\"currDate\" [(ngModel)]='empSheet.WorkingDate' />\r\n\r\n    </div>\r\n</div>\r\n<div class=\"row mt-2\">\r\n\r\n    <div class=\"col-lg-1\"><label class=\"label-control\">Hours:</label></div>\r\n\r\n\r\n    <div class=\"col-lg-4\">\r\n        <input type=\"number\" class=\"form-control\" [(ngModel)]='empSheet.WorkingHours' />\r\n    </div>\r\n\r\n</div>\r\n\r\n<div class=\"row mt-2\">\r\n    <div class=\"col-lg-5 text-center\"><input type=\"button\" class=\"btn btn-primary\" (click)='saveSheet()' value=\"Save\" />\r\n    </div>\r\n</div>\r\n</div>"

/***/ }),

/***/ "./src/app/AddTimeLog/addtimelog.component.scss":
/*!******************************************************!*\
  !*** ./src/app/AddTimeLog/addtimelog.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/AddTimeLog/addtimelog.component.ts":
/*!****************************************************!*\
  !*** ./src/app/AddTimeLog/addtimelog.component.ts ***!
  \****************************************************/
/*! exports provided: AddTimeLogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddTimeLogComponent", function() { return AddTimeLogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/employee.service */ "./src/app/services/employee.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AddTimeLogComponent = /** @class */ (function () {
    function AddTimeLogComponent(employeeService, activeRoute, _router) {
        this.employeeService = employeeService;
        this.activeRoute = activeRoute;
        this._router = _router;
        this.employeesList = [];
        this.taskList = [];
        this.empSheet = {};
        this.currDate = new Date().toISOString().substring(0, 10);
    }
    AddTimeLogComponent.prototype.ngOnInit = function () {
        this.initEmpTimesheetDetail();
        this.getAllEmployees();
        this.getEmpTasks();
    };
    AddTimeLogComponent.prototype.initEmpTimesheetDetail = function () {
        this.currEmpId = Number(this.activeRoute.snapshot.params["empId"]);
        this.empSheet.EmployeeId = this.currEmpId;
        this.empSheet.TaskId = 1;
    };
    AddTimeLogComponent.prototype.getAllEmployees = function () {
        var _this = this;
        this.employeeService.getallemployees().subscribe(function (data) { return _this.employeesList = data; });
    };
    AddTimeLogComponent.prototype.getEmpTasks = function () {
        var _this = this;
        this.employeeService.getalltasks().subscribe(function (data) { return _this.taskList = data; });
    };
    AddTimeLogComponent.prototype.getEmpSheet = function (id) {
        var _this = this;
        this.employeeService.getEmployeeSheets(id).subscribe(function (data) { return _this.sheets = data; });
    };
    AddTimeLogComponent.prototype.saveSheet = function () {
        var _this = this;
        this.employeeService.saveNewSheet(this.empSheet).subscribe(function (data) {
            _this._router.navigate(['/timesheet', _this.empSheet.EmployeeId]);
        });
    };
    AddTimeLogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'newtimesheet',
            template: __webpack_require__(/*! ./addtimelog.component.html */ "./src/app/AddTimeLog/addtimelog.component.html"),
            styles: [__webpack_require__(/*! ./addtimelog.component.scss */ "./src/app/AddTimeLog/addtimelog.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_employee_service__WEBPACK_IMPORTED_MODULE_1__["EmployeeService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AddTimeLogComponent);
    return AddTimeLogComponent;
}());



/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--<employee-list></employee-list>-->\r\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'app';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _employee_employee_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./employee/employee.component */ "./src/app/employee/employee.component.ts");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/employee.service */ "./src/app/services/employee.service.ts");
/* harmony import */ var _timesheet_timesheet_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./timesheet/timesheet.component */ "./src/app/timesheet/timesheet.component.ts");
/* harmony import */ var _AddTimeLog_addtimelog_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./AddTimeLog/addtimelog.component */ "./src/app/AddTimeLog/addtimelog.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








// import {AppRoutingModule} from './app-routing-module'


//Routes
var appRoutes = [
    { path: '', redirectTo: '/employeelist', pathMatch: 'full' },
    { path: 'employeelist', component: _employee_employee_component__WEBPACK_IMPORTED_MODULE_6__["EmployeeListComponent"] },
    { path: 'timesheet/:empId', component: _timesheet_timesheet_component__WEBPACK_IMPORTED_MODULE_8__["TimesheetComponent"] },
    { path: 'newtimesheet/:empId', component: _AddTimeLog_addtimelog_component__WEBPACK_IMPORTED_MODULE_9__["AddTimeLogComponent"] },
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _employee_employee_component__WEBPACK_IMPORTED_MODULE_6__["EmployeeListComponent"],
                _timesheet_timesheet_component__WEBPACK_IMPORTED_MODULE_8__["TimesheetComponent"],
                _AddTimeLog_addtimelog_component__WEBPACK_IMPORTED_MODULE_9__["AddTimeLogComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                // AppRoutingModule,
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forRoot(appRoutes)
            ],
            providers: [
                _services_employee_service__WEBPACK_IMPORTED_MODULE_7__["EmployeeService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/employee/employee.component.html":
/*!**************************************************!*\
  !*** ./src/app/employee/employee.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Employee List</h1>\r\n<table class=\"table\">\r\n    <tr>\r\n        <th>ID</th>\r\n        <th>Code</th>\r\n        <th>Name</th>\r\n        <th>Weekly Effort (Total)</th>\r\n        <th>Weekly Effort (Average)</th>\r\n    </tr>\r\n    <tr *ngFor=\"let item of employees\" class=\"pointer\" [routerLink]=\"['/timesheet', item.id]\" title='show details'>\r\n        <td >{{item.id}}</td>\r\n        <td>{{item.code}}</td>\r\n        <td>{{item.name}}</td>\r\n         <td>{{item.weeklyTotal}}</td>\r\n        <td>{{item.weeklyAvrage}}</td>\r\n       \r\n    </tr>\r\n</table>\r\n"

/***/ }),

/***/ "./src/app/employee/employee.component.scss":
/*!**************************************************!*\
  !*** ./src/app/employee/employee.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1 {\n  color: grey; }\n\n.pointer {\n  cursor: pointer; }\n\n.table {\n  border-collapse: collapse;\n  width: 100%; }\n\n.table td, .table th {\n    border: 1px solid #ddd;\n    padding: 7px; }\n\n.table tr:nth-child(even) {\n    background-color: #f2f2f2; }\n\n.table th {\n    padding-top: 12px;\n    padding-bottom: 12px;\n    text-align: left;\n    background-color: #1c6ca5;\n    color: white; }\n\n.table tr:hover {\n    background-color: #ddd; }\n"

/***/ }),

/***/ "./src/app/employee/employee.component.ts":
/*!************************************************!*\
  !*** ./src/app/employee/employee.component.ts ***!
  \************************************************/
/*! exports provided: EmployeeListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeListComponent", function() { return EmployeeListComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/employee.service */ "./src/app/services/employee.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var EmployeeListComponent = /** @class */ (function () {
    function EmployeeListComponent(employeeService) {
        this.employeeService = employeeService;
    }
    EmployeeListComponent.prototype.ngOnInit = function () {
        this.getAllEmployees();
    };
    EmployeeListComponent.prototype.getAllEmployees = function () {
        var _this = this;
        this.employeeService.getallemployees().subscribe(function (data) { return _this.employees = data; });
    };
    EmployeeListComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'employee-list',
            template: __webpack_require__(/*! ./employee.component.html */ "./src/app/employee/employee.component.html"),
            styles: [__webpack_require__(/*! ./employee.component.scss */ "./src/app/employee/employee.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_employee_service__WEBPACK_IMPORTED_MODULE_1__["EmployeeService"]])
    ], EmployeeListComponent);
    return EmployeeListComponent;
}());



/***/ }),

/***/ "./src/app/services/employee.service.ts":
/*!**********************************************!*\
  !*** ./src/app/services/employee.service.ts ***!
  \**********************************************/
/*! exports provided: EmployeeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmployeeService", function() { return EmployeeService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EmployeeService = /** @class */ (function () {
    function EmployeeService(http) {
        this.http = http;
        this.baseapi = src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_1__["environment"].apiUrl;
    }
    EmployeeService.prototype.getallemployees = function () {
        return this.http.get(this.baseapi + "/employee/getall");
    };
    EmployeeService.prototype.getEmployeeSheets = function (empID) {
        return this.http.get(this.baseapi + "/employee/GetEmployeeTimeSheetByID/" + empID);
    };
    EmployeeService.prototype.getalltasks = function () {
        return this.http.get(this.baseapi + "/employee/getalltasks");
    };
    EmployeeService.prototype.saveNewSheet = function (model) {
        return this.http.post(this.baseapi + "/employee/saveEmployeeTaskSheet", model);
    };
    EmployeeService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], EmployeeService);
    return EmployeeService;
}());



/***/ }),

/***/ "./src/app/timesheet/timesheet.component.html":
/*!****************************************************!*\
  !*** ./src/app/timesheet/timesheet.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"mb-2\">\r\n    <h1>Employee Sheet:</h1>\r\n</div>\r\n<div class=\"row mt-2 mb-2\">\r\n    <div class=\"col-lg-1\"><label class=\"label-control\">Employee:</label></div>\r\n    <div class=\"col-lg-4\">\r\n        <select [(ngModel)]='currEmpId' class=\"form-control\" (ngModelChange)='onSelectedModelChange($event)'>\r\n    <option *ngFor=\"let emp of employeeList\" [ngValue]=\"emp.id\"  [selected]=\"emp.id == optionSelected\">\r\n        {{emp.name}}\r\n    </option>\r\n</select>\r\n    </div>\r\n    <div class=\"col-lg-7 text-right\"><button class=\"btn btn-link\" [routerLink]=\"['/newtimesheet', currEmpId]\">Add Time Log</button>\r\n    </div>\r\n</div>\r\n\r\n<table class=\"table table-stripped\">\r\n    <tr class=\"table-primary\">\r\n        <th>Task</th>\r\n        <th>Monday</th>\r\n        <th>Tuesday</th>\r\n        <th>Wenesday</th>\r\n        <th>Thursday</th>\r\n        <th>Friday</th>\r\n        <th>Saturday</th>\r\n        <th>Sunday</th>\r\n\r\n    </tr>\r\n    <tr *ngFor=\"let sheet of sheets\">\r\n        <td>{{sheet.task}}</td>\r\n        <td>{{sheet.mondayHours}}</td>\r\n        <td>{{sheet.tuesdayHours}}</td>\r\n        <td>{{sheet.wenesdayHours}}</td>\r\n        <td>{{sheet.thursdayHours}}</td>\r\n        <td>{{sheet.fridayHours}}</td>\r\n        <td>{{sheet.saturdayHours}}</td>\r\n        <td>{{sheet.sundayHours}}</td>\r\n    </tr>\r\n    <tr *ngIf=\"sheets.length\">\r\n        <td>Total</td>\r\n        <td>{{getColSum('mondayHours')}}</td>\r\n        <td>{{getColSum('tuesdayHours')}}</td>\r\n        <td>{{getColSum('wenesdayHours')}}</td>\r\n        <td>{{getColSum('thursdayHours')}}</td>\r\n        <td>{{getColSum('fridayHours')}}</td>\r\n        <td>{{getColSum('saturdayHours')}}</td>\r\n        <td>{{getColSum('sundayHours')}}</td>\r\n    </tr>\r\n    <tr *ngIf=\"!sheets.length\">\r\n        <td colspan=\"8\">No record found!</td>\r\n    </tr>\r\n</table>\r\n\r\n<div class=\"text-right mt-5\">\r\n    <a class=\"btn btn-link\" routerLink='/employeelist'>View Employee List</a>\r\n    <div>"

/***/ }),

/***/ "./src/app/timesheet/timesheet.component.scss":
/*!****************************************************!*\
  !*** ./src/app/timesheet/timesheet.component.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/timesheet/timesheet.component.ts":
/*!**************************************************!*\
  !*** ./src/app/timesheet/timesheet.component.ts ***!
  \**************************************************/
/*! exports provided: TimesheetComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimesheetComponent", function() { return TimesheetComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_employee_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/employee.service */ "./src/app/services/employee.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TimesheetComponent = /** @class */ (function () {
    function TimesheetComponent(employeeService, router, activeRoute) {
        this.employeeService = employeeService;
        this.router = router;
        this.activeRoute = activeRoute;
        this.sheets = [];
        this.employeeList = [];
    }
    ;
    TimesheetComponent.prototype.ngOnInit = function () {
        this.initEmpTimesheetDetail();
        this.getAllEmployees();
    };
    TimesheetComponent.prototype.initEmpTimesheetDetail = function () {
        this.currEmpId = Number(this.activeRoute.snapshot.params["empId"]);
        this.getEmpSheetbyID(this.currEmpId);
    };
    TimesheetComponent.prototype.getAllEmployees = function () {
        var _this = this;
        this.employeeService.getallemployees().subscribe(function (data) {
            return _this.employeeList = data;
        });
    };
    TimesheetComponent.prototype.getEmpSheetbyID = function (id) {
        var _this = this;
        this.employeeService.getEmployeeSheets(id).subscribe(function (data) {
            return _this.sheets = data;
        });
    };
    TimesheetComponent.prototype.onSelectedModelChange = function (event) {
        this.router.navigate(['/timesheet', event]);
        this.getEmpSheetbyID(event);
    };
    TimesheetComponent.prototype.getColSum = function (key) {
        var total = 0;
        this.sheets.forEach(function (element) {
            total += element[key];
        });
        return total;
    };
    TimesheetComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'timesheet',
            template: __webpack_require__(/*! ./timesheet.component.html */ "./src/app/timesheet/timesheet.component.html"),
            styles: [__webpack_require__(/*! ./timesheet.component.scss */ "./src/app/timesheet/timesheet.component.scss")]
        }),
        __metadata("design:paramtypes", [_services_employee_service__WEBPACK_IMPORTED_MODULE_1__["EmployeeService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]])
    ], TimesheetComponent);
    return TimesheetComponent;
}());



/***/ }),

/***/ "./src/environments/environment.prod.ts":
/*!**********************************************!*\
  !*** ./src/environments/environment.prod.ts ***!
  \**********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var environment = {
    production: true,
    apiUrl: "https://localhost:44391/api/v1"
};


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\alamz\Source\Repos\timesheet2\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map