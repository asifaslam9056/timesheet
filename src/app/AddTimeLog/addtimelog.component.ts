import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'newtimesheet',
  templateUrl: './addtimelog.component.html',
  styleUrls: ['./addtimelog.component.scss']
})
export class AddTimeLogComponent implements OnInit {
  currEmpId: number;
  sheets: any;
  employeesList: any = [];
  taskList: any = [];
  empSheet: any = {};
  currDate = new Date().toISOString().substring(0, 10);
  constructor(private employeeService: EmployeeService, public activeRoute: ActivatedRoute, public _router: Router) {
  }

  ngOnInit() {
    this.initEmpTimesheetDetail();
    this.getAllEmployees();
    this.getEmpTasks();
  }

  initEmpTimesheetDetail() {
    this.currEmpId = Number(this.activeRoute.snapshot.params["empId"]);
    this.empSheet.EmployeeId = this.currEmpId;
    this.empSheet.TaskId = 1;
  }

  getAllEmployees() { this.employeeService.getallemployees().subscribe(data => this.employeesList = data) }

  getEmpTasks() { this.employeeService.getalltasks().subscribe(data => this.taskList = data) }

  getEmpSheet(id) { this.employeeService.getEmployeeSheets(id).subscribe(data => this.sheets = data) }

  saveSheet() {
    this.employeeService.saveNewSheet(this.empSheet).subscribe(data => {
      this._router.navigate(['/timesheet', this.empSheet.EmployeeId])
    });

  }

}
