import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule} from '@angular/forms'
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { EmployeeListComponent } from './employee/employee.component';
import { EmployeeService } from './services/employee.service';
import { TimesheetComponent } from './timesheet/timesheet.component'
import { AddTimeLogComponent } from './AddTimeLog/addtimelog.component'


//Routes

const routes: Routes = [
    {path:'',redirectTo:'/employeelist',pathMatch:'full'},
    { path: 'employeelist', component: EmployeeListComponent },
    { path: 'timesheet/:empId', component: TimesheetComponent },
    { path: 'addtimelog/:empId', component: AddTimeLogComponent },
];



@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    TimesheetComponent,
    AddTimeLogComponent  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    EmployeeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
