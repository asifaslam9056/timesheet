import { Component } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.scss']
})
export class TimesheetComponent {
  currEmpId: number;
  sheets: any = [];
  employeeList: any = [];;
  constructor(private employeeService: EmployeeService, public router: Router, public activeRoute: ActivatedRoute) {

  }

  ngOnInit() {
    this.initEmpTimesheetDetail();
    this.getAllEmployees();
  }

  initEmpTimesheetDetail() {
    this.currEmpId = Number(this.activeRoute.snapshot.params["empId"]);
    this.getEmpSheetbyID(this.currEmpId);
  }

  getAllEmployees() {
    this.employeeService.getallemployees().subscribe(data =>
      this.employeeList = data
    );
  }

  getEmpSheetbyID(id) {
    this.employeeService.getEmployeeSheets(id).subscribe(data =>
      this.sheets = data
    );
  }

  onSelectedModelChange(event) {
    this.router.navigate(['/timesheet', event]);
    this.getEmpSheetbyID(event);
  }


  getColSum(key: string): number {
    let total = 0;
    this.sheets.forEach(element => {
      total += element[key];
    });
    return total;
  }

}
