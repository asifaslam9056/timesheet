import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EmployeeService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getallemployees() {
        return this.http.get(this.baseapi + "/employee/getall");
    }

    getEmployeeSheets(empID:number){
        return this.http.get(this.baseapi + "/employee/GetEmployeeTimeSheetByID/"+empID);
    }

    getalltasks(){
         return this.http.get(this.baseapi + "/employee/getalltasks");
    }

    saveNewSheet(model:any){
        return this.http.post(this.baseapi + "/employee/saveEmployeeTaskSheet",model)
    }
}